package com.botdy.gdghack.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nguyenbinh on 11/18/17.
 */

public class ConfirmInfo extends BaseObservable implements Parcelable {

    @Bindable
    public Table table;
    @Bindable
    public String date;
    @Bindable
    public String time;
    @Bindable
    public String name;
    @Bindable
    public String email;
    @Bindable
    public String phone;
    @Bindable
    public String dishid;

    public void setTable(Table table) {
        this.table = table;

    }

    public void setDate(String date) {
        this.date = date;
        notifyChange();
    }

    public void setTime(String time) {
        this.time = time;
        notifyChange();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public ConfirmInfo() {
        date = "";
        time = "";
        name = "";
        email = "";
        phone = "";
    }

    protected ConfirmInfo(Parcel in) {
        table = in.readParcelable(Table.class.getClassLoader());
        date = in.readString();
        time = in.readString();
        name = in.readString();
        email = in.readString();
        phone = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(table, flags);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(phone);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ConfirmInfo> CREATOR = new Creator<ConfirmInfo>() {
        @Override
        public ConfirmInfo createFromParcel(Parcel in) {
            return new ConfirmInfo(in);
        }

        @Override
        public ConfirmInfo[] newArray(int size) {
            return new ConfirmInfo[size];
        }
    };
}
