package com.botdy.gdghack.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by daugau on 18/11/2017.
 */

public class DishInfo {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("addr")
    @Expose
    private String addr;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("urlImage")
    @Expose
    private String urlImage;

    public String getId() {
        return id;
    }

    public String getAddr() {
        return addr;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getUrlImage() {
        return urlImage;
    }
}
