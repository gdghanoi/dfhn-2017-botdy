package com.botdy.gdghack.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by daugau on 18/11/2017.
 */

public class PaymentRequestInfo extends UserRequestInfo {

    @SerializedName("tableId")
    @Expose
    private String tableId;

    @SerializedName("dishId")
    @Expose
    private String dishId;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("time")
    @Expose
    private String time;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("cardNo")
    @Expose
    private String cardNo;

    @SerializedName("expiry")
    @Expose
    private String expiry;

    @SerializedName("ccv")
    @Expose
    private String ccv;

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public void setDishId(String dishId) {
        this.dishId = dishId;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public void setCcv(String ccv) {
        this.ccv = ccv;
    }
}
