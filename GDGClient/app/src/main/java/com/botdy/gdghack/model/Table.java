package com.botdy.gdghack.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.botdy.gdghack.BR;

/**
 * Created by nguyenbinh on 11/18/17.
 */

public class Table extends BaseObservable implements Parcelable{
    @Bindable
    public String index;
    @Bindable
    public int status;
    @Bindable
    public String floor;
    @Bindable
    public String numSeat;
    @Bindable
    public String description;

    public Table(String index, int status, String floor, String numSeat, String description) {
        this.index = index;
        this.status = status;
        this.floor = floor;
        this.numSeat = numSeat;
        this.description = description;
    }

    protected Table(Parcel in) {
        index = in.readString();
        status = in.readInt();
        floor = in.readString();
        numSeat = in.readString();
        description = in.readString();
    }

    public static final Creator<Table> CREATOR = new Creator<Table>() {
        @Override
        public Table createFromParcel(Parcel in) {
            return new Table(in);
        }

        @Override
        public Table[] newArray(int size) {
            return new Table[size];
        }
    };

    public void setIndex(String index) {
        this.index = index;
        notifyChange();
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public void setNumSeat(String numSeat) {
        this.numSeat = numSeat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(index);
        parcel.writeInt(status);
        parcel.writeString(floor);
        parcel.writeString(numSeat);
        parcel.writeString(description);
    }
}
