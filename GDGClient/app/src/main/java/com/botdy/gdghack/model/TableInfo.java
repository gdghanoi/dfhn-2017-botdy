package com.botdy.gdghack.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by daugau on 18/11/2017.
 */

public class TableInfo {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("floorInfo")
    @Expose
    private String floorInfo;

    @SerializedName("numberOfseet")
    @Expose
    private Integer numberOfSeet;

    @SerializedName("status")
    @Expose
    private Integer status;

    @SerializedName("vipValue")
    @Expose
    private String vipValue;

    public String getId() {
        return id;
    }

    public String getFloorInfo() {
        return floorInfo;
    }

    public Integer getNumberOfSeet() {
        return numberOfSeet;
    }

    public Integer getStatus() {
        return status;
    }

    public String getVipValue() {
        return vipValue;
    }
}
