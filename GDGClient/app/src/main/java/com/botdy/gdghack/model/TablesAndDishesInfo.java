package com.botdy.gdghack.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by daugau on 18/11/2017.
 */

public class TablesAndDishesInfo {

    @SerializedName("tables")
    @Expose
    private List<TableInfo> tables;

    @SerializedName("dishes")
    @Expose
    private List<DishInfo> dishes;

    public List<TableInfo> getTables() {
        return tables;
    }

    public List<DishInfo> getDishes() {
        return dishes;
    }
}
