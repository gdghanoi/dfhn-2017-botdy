package com.botdy.gdghack.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by daugau on 18/11/2017.
 */

public class UserResponseInfo {

    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("token")
    @Expose
    private String token;

    public Boolean getStatus() {
        return status;
    }

    public String getToken() {
        return token;
    }

    public String toString() {
        return "status: " + status + " token: " + token;
    }
}
