package com.botdy.gdghack.remote;

import com.botdy.gdghack.model.PaymentRequestInfo;
import com.botdy.gdghack.model.PaymentResponseInfo;
import com.botdy.gdghack.model.TablesAndDishesInfo;
import com.botdy.gdghack.model.UserLoginInfo;
import com.botdy.gdghack.model.UserRequestInfo;
import com.botdy.gdghack.model.UserResponseInfo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by daugau on 18/11/2017.
 */

public interface APIService {

    @POST("/gdg_hackathon/login")
    Call<UserResponseInfo> login(@Body UserLoginInfo info);

    @POST("/gdg_hackathon/list-table")
    Call<TablesAndDishesInfo> getListTables(@Body UserRequestInfo info);

    @POST("/gdg_hackathon/payment")
    Call<PaymentResponseInfo> pay(@Body PaymentRequestInfo info);
}
