package com.botdy.gdghack.remote;

/**
 * Created by daugau on 18/11/2017.
 */

public class ApiUtils {

    private ApiUtils() {}

    public static final String BASE_URL = "http://35.187.238.109:8000/";

    public static APIService getAPIService() {
        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}
