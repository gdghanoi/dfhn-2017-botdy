package com.botdy.gdghack.view.activity;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.botdy.gdghack.R;
import com.botdy.gdghack.databinding.ConfirmDataBinding;
import com.botdy.gdghack.databinding.TableDataBinding;
import com.botdy.gdghack.viewmodel.ConfirmModelView;
import com.botdy.gdghack.viewmodel.TableModelView;

public class ConfirmActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConfirmDataBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_confirm);
        ConfirmModelView modelView = new ConfirmModelView();
        binding.setCfMV(modelView);
    }
}
