package com.botdy.gdghack.view.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.botdy.gdghack.R;
import com.botdy.gdghack.databinding.FillInfoDataBinding;
import com.botdy.gdghack.databinding.TableDataBinding;
import com.botdy.gdghack.model.ConfirmInfo;
import com.botdy.gdghack.model.Table;
import com.botdy.gdghack.viewmodel.FillInfoModelView;
import com.botdy.gdghack.viewmodel.TableModelView;

public class FillInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConfirmInfo confirmInfo = new ConfirmInfo();
        FillInfoDataBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_fill_info);
        FillInfoModelView modelView = new FillInfoModelView(this, confirmInfo);
        binding.setFiMV(modelView);
        binding.setConfirmInfo(confirmInfo);

        Bundle data = getIntent().getExtras();
        if (data != null) {
            Table table = data.getParcelable("table");
            binding.setTable(table);
        }
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.dish, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        binding.spinner.setAdapter(adapter);
    }
}
