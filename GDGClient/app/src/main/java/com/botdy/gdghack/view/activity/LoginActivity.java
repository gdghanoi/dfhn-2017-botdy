package com.botdy.gdghack.view.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.botdy.gdghack.R;
import com.botdy.gdghack.model.UserLoginInfo;
import com.botdy.gdghack.model.UserResponseInfo;
import com.botdy.gdghack.remote.APIService;
import com.botdy.gdghack.remote.ApiUtils;

import java.security.MessageDigest;
import java.util.Formatter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private APIService mAPIService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAPIService = ApiUtils.getAPIService();
    }

    public void login(View view) {
        EditText usernameEt = findViewById(R.id.et_username);
        EditText passwordEt = findViewById(R.id.et_password);

        String username = usernameEt.getText().toString().trim();
        String password = passwordEt.getText().toString().trim();

        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            return;
        }

        String shaPassword = hashPassword(password);
        check(username, shaPassword);
    }

    private void check(final String username, String shaPassword) {
        UserLoginInfo info = new UserLoginInfo();
        info.setUsername(username);
        info.setPassword(shaPassword);

        mAPIService.login(info).enqueue(new Callback<UserResponseInfo>() {
            @Override
            public void onResponse(Call<UserResponseInfo> call, Response<UserResponseInfo> response) {
                if (response.isSuccessful()) {
                    UserResponseInfo responseInfo = response.body();

                    if (responseInfo.getStatus()) {
                        saveUserInfo(username, responseInfo.getToken());
                        nextIntent();
                    } else {
                        Toast.makeText(LoginActivity.this,
                                "Thông tin bạn điền chưa đúng",
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e("Loi", Integer.toString(response.code()));
                    Toast.makeText(LoginActivity.this,
                            "Hãy kiểm tra lại kết nối mạng",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserResponseInfo> call, Throwable t) {
                Toast.makeText(LoginActivity.this,
                        "Hãy kiểm tra lại kết nối mạng",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void nextIntent() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private static String hashPassword(String password) {
        String sha1 = "";
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.reset();
            md.update(password.getBytes("UTF-8"));
            Formatter formatter = new Formatter();
            for (byte b : md.digest()) {
                formatter.format("%02x", b);
            }
            sha1 = formatter.toString();
            formatter.close();
        } catch(Exception ex) {
            Log.e("Error", ex.toString());
        }
        return sha1;
    }

    private void saveUserInfo(String username, String userToken) {
        SharedPreferences preferences = getSharedPreferences("user_info", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        Log.i("username", username);
        Log.i("user_token", userToken);

        editor.putString("username", username);
        editor.putString("user_token", userToken);
        editor.apply();
    }
}
