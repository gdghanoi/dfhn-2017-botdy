package com.botdy.gdghack.view.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.botdy.gdghack.R;
import com.botdy.gdghack.model.ConfirmInfo;
import com.botdy.gdghack.model.PaymentRequestInfo;
import com.botdy.gdghack.model.PaymentResponseInfo;
import com.botdy.gdghack.model.UserRequestInfo;
import com.botdy.gdghack.remote.APIService;
import com.botdy.gdghack.remote.ApiUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentActivity extends AppCompatActivity {

    private ConfirmInfo data;
    private APIService mAPIService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        mAPIService = ApiUtils.getAPIService();
        data = getIntent().getExtras().getParcelable("confirm");
    }

    public void pay(View view) {
        PaymentRequestInfo requestInfo = new PaymentRequestInfo();

        EditText etCardNo = findViewById(R.id.et_cardno);
        EditText etExpire = findViewById(R.id.editText2);
        EditText etCcv = findViewById(R.id.et_ccv);

        SharedPreferences preferences = getSharedPreferences("user_info", MODE_PRIVATE);
        String username = preferences.getString("username", "");
        String userToken = preferences.getString("user_token", "");

        requestInfo.setUsername(username);
        requestInfo.setToken(userToken);

        requestInfo.setCardNo(etCardNo.getText().toString().trim());
        requestInfo.setCcv(etCcv.getText().toString().trim());
        requestInfo.setDate(data.date);
        requestInfo.setTime(data.time);
        requestInfo.setEmail(data.email);
        requestInfo.setExpiry(etExpire.getText().toString().trim());
        requestInfo.setPhone(data.phone);
        requestInfo.setTableId("-KzCyUmnKtsi0kIT4fHE");
        requestInfo.setDishId(data.dishid);

        mAPIService.pay(requestInfo).enqueue(new Callback<PaymentResponseInfo>() {
            @Override
            public void onResponse(Call<PaymentResponseInfo> call, Response<PaymentResponseInfo> response) {
                if (response.isSuccessful()) {
                    PaymentResponseInfo info = response.body();
                    if (info.getStatus()) {
                        Toast.makeText(PaymentActivity.this,
                                "Thanh toán thành công!",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(PaymentActivity.this,
                                "Thanh toán thất bại!",
                                Toast.LENGTH_SHORT).show();
                    }

                    nextIntent();
                } else {
                    Toast.makeText(PaymentActivity.this,
                            "Hãy kiểm tra lại kết nối mạng",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PaymentResponseInfo> call, Throwable t) {
                Toast.makeText(PaymentActivity.this,
                        "Hãy kiểm tra lại kết nối mạng",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void nextIntent() {
        Intent intent = new Intent(PaymentActivity.this, TableActivity.class);
        startActivity(intent);
        finish();
    }
}
