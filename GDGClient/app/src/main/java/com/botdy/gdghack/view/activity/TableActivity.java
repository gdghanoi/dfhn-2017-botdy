package com.botdy.gdghack.view.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.botdy.gdghack.R;
import com.botdy.gdghack.databinding.TableDataBinding;
import com.botdy.gdghack.model.Table;
import com.botdy.gdghack.model.TableInfo;
import com.botdy.gdghack.model.TablesAndDishesInfo;
import com.botdy.gdghack.model.UserRequestInfo;
import com.botdy.gdghack.remote.APIService;
import com.botdy.gdghack.remote.ApiUtils;
import com.botdy.gdghack.viewmodel.RecyclerTableAdapter;
import com.botdy.gdghack.viewmodel.TableModelView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TableActivity extends AppCompatActivity {

    private TableDataBinding binding;
    private APIService mAPIService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_table);

        mAPIService = ApiUtils.getAPIService();
        loadTable();
    }

    private void loadTable() {
        SharedPreferences preferences = getSharedPreferences("user_info", MODE_PRIVATE);
        String username = preferences.getString("username", "");
        String userToken = preferences.getString("user_token", "");


        UserRequestInfo requestInfo = new UserRequestInfo();
        requestInfo.setUsername(username);
        requestInfo.setToken(userToken);
        mAPIService.getListTables(requestInfo).enqueue(new Callback<TablesAndDishesInfo>() {
            @Override
            public void onResponse(Call<TablesAndDishesInfo> call, Response<TablesAndDishesInfo> response) {
                if (response.isSuccessful()) {
                    TablesAndDishesInfo info = response.body();
                    displayTables(info.getTables());
                } else {
                    Toast.makeText(TableActivity.this,
                            "Hãy kiểm tra lại kết nối mạng",
                            Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TablesAndDishesInfo> call, Throwable t) {
                Toast.makeText(TableActivity.this,
                        "Hãy kiểm tra lại kết nối mạng",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void displayTables(List<TableInfo> tables) {
        TableModelView modelView = new TableModelView(this);
        binding.setTbMV(modelView);

        ArrayList<Table> listTable = new ArrayList<>();

        for (int i = 0; i < tables.size(); i++) {
            String index = Integer.toString(i + 1);
            int status = tables.get(i).getStatus();
            String floor = tables.get(i).getFloorInfo();
            String seet = tables.get(i).getNumberOfSeet().toString();
            String description = tables.get(i).getVipValue();
            listTable.add(new Table(index, status, floor, seet, description));
        }

        RecyclerTableAdapter adapter = new RecyclerTableAdapter(this, listTable, modelView);
        binding.recyclerTableView.setAdapter(adapter);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        binding.recyclerTableView.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.logout:
                SharedPreferences preferences = getSharedPreferences("user_info", MODE_PRIVATE);
                preferences.edit().clear().apply();
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                return true;
            case R.id.refresh:
                loadTable();
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
