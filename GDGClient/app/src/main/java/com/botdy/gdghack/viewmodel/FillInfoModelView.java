package com.botdy.gdghack.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.botdy.gdghack.model.ConfirmInfo;
import com.botdy.gdghack.view.activity.FillInfoActivity;
import com.botdy.gdghack.view.activity.PaymentActivity;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

/**
 * Created by nguyenbinh on 11/18/17.
 */

public class FillInfoModelView implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener{
    private FillInfoActivity context;
    private ConfirmInfo confirmInfo;
    public FillInfoModelView(FillInfoActivity context, ConfirmInfo confirmInfo){
        this.context = context;
        this.confirmInfo = confirmInfo;
    }
    public void editTime(View view){
        Calendar now = Calendar.getInstance();
        TimePickerDialog dpd = TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true
        );
        dpd.show(context.getFragmentManager(), "Timepickerdialog");
    }

    public void checkout(View view){
        Intent paymentIntent = new Intent(context, PaymentActivity.class);
        paymentIntent.putExtra("confirm", confirmInfo);
        context.startActivity(paymentIntent);
    }

    public void editDate(View view){
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(context.getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        confirmInfo.setDate(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        confirmInfo.setTime(hourOfDay+"h"+minute+"m");
    }


    public void onUsernameTextChanged(CharSequence text) {
        confirmInfo.setName(text.toString());
    }

    public void onEmailTextChanged(CharSequence text) {
        confirmInfo.setEmail(text.toString());
    }

    public void onPhoneTextChanged(CharSequence text) {
        confirmInfo.setPhone(text.toString());
    }

}
