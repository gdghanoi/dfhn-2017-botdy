package com.botdy.gdghack.viewmodel;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.botdy.gdghack.R;
import com.botdy.gdghack.databinding.TableDataBinding;
import com.botdy.gdghack.databinding.TableItemBinding;
import com.botdy.gdghack.model.Table;

import java.util.ArrayList;

/**
 * Created by nguyenbinh on 11/18/17.
 */

public class RecyclerTableAdapter extends RecyclerView.Adapter<RecyclerTableAdapter.TableViewHolder> {
    private Context context;
    private ArrayList<Table> listTable;
    private TableModelView tableMV ;
    public RecyclerTableAdapter(Context context, ArrayList<Table> listTable, TableModelView tableMV ) {
        this.context = context;
        this.listTable = listTable;
        this.tableMV = tableMV;
    }


    @Override
    public TableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            TableItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.table_item_layout, parent, false);
            return new TableViewHolder(binding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(TableViewHolder holder, int position) {
        TableItemBinding binding = holder.getBind();
        binding.setTable(listTable.get(position));
        binding.setTbMV(tableMV);
    }

    @Override
    public int getItemCount() {
        return listTable.size();
    }

    public class TableViewHolder extends RecyclerView.ViewHolder {
        private TableItemBinding binding;

        public TableViewHolder(TableItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public TableItemBinding getBind(){
            return binding;
        }
    }
}
