package com.botdy.gdghack.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.botdy.gdghack.model.Table;
import com.botdy.gdghack.view.activity.FillInfoActivity;

/**
 * Created by nguyenbinh on 11/18/17.
 */

public class TableModelView {
    private Context context;
    public TableModelView(Context context){
        this.context = context;
    }

    public void clickTable(View view, Table table){
        Intent fillInfoIntent = new Intent(context, FillInfoActivity.class);
        fillInfoIntent.putExtra("table", table);
        context.startActivity(fillInfoIntent);
    }



}
