package com.tech.database;

import io.vertx.core.json.JsonObject;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FirebaseConnector {

    private HttpClient client;
    private final String FILEBASE_URL = "https://gdghackathon-13935.firebaseio.com/";
    private final String AUTH_TOKEN = "sxkDoSZKujRHt48bount0u8JfVxTzLgk7pUkorPc";

    public FirebaseConnector() {
        client = HttpClientBuilder.create().build();
    }

    public String put(String tableName, String id, String json) {
        // set firebase url
        String url = FILEBASE_URL + tableName + "/" + id + ".json?auth=" + AUTH_TOKEN;
        HttpPut httpPut = new HttpPut(url);
        httpPut.addHeader("User-Agent", "Mozilla/5.0");

        // request
        httpPut.setEntity(new StringEntity(json, "UTF-8"));
        try {
            HttpResponse response = client.execute(httpPut);
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuilder result = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "ERROR!!!";
    }

    public String put(String tableName, String json) {
        // set firebase url
        String url = FILEBASE_URL + tableName + ".json?auth=" + AUTH_TOKEN;
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader("User-Agent", "Mozilla/5.0");

        // request
        httpPost.setEntity(new StringEntity(json, "UTF-8"));
        try {
            HttpResponse response = client.execute(httpPost);
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuilder result = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "ERROR!!!";
    }

    public String scan(String tableName){
        String url = FILEBASE_URL + tableName + ".json?auth=" + AUTH_TOKEN;
        HttpGet httpGet = new HttpGet(url);
        httpGet.addHeader("User-Agent", "Mozilla/5.0");
        try {
            HttpResponse response = client.execute(httpGet);
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuilder result = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "ERROR!!!";
    }

    public String deleteTable(String tableName){
        String url = FILEBASE_URL + tableName + ".json?auth=" + AUTH_TOKEN;
        HttpDelete httpDelete = new HttpDelete(url);
        httpDelete.addHeader("User-Agent", "Mozilla/5.0");
        try {
            HttpResponse response = client.execute(httpDelete);
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuilder result = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "ERROR!!!";
    }

    public String take(String tableName, String id){
        String url = FILEBASE_URL + tableName + ".json?auth=" + AUTH_TOKEN;
        HttpGet httpGet = new HttpGet(url);
        httpGet.addHeader("User-Agent", "Mozilla/5.0");
        try {
            HttpResponse response = client.execute(httpGet);
            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuilder result = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            JsonObject jsonObject = new JsonObject(result.toString());
            return jsonObject.getJsonObject(id).toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "ERROR!!!";
    }

    public static void main(String[] args) {
        FirebaseConnector firebaseConnector = new FirebaseConnector();
        JsonObject jsonObject1 = new JsonObject();
        jsonObject1.put("username", "1");
        jsonObject1.put("password", "quang");
//        System.out.println(firebaseConnector.put("User", "quang1", jsonObject1.toString()));
//        System.out.println(firebaseConnector.put("User", "quang2", jsonObject1.toString()));
        System.out.println(firebaseConnector.put("test", jsonObject1.toString()));

//        JsonObject jsonObject2 = new JsonObject();
//        jsonObject2.put("id", "1");
//        jsonObject2.put("name", "quang");
//        System.out.println(firebaseConnector.put("user", jsonObject2.toString()));
//        System.out.println(firebaseConnector.scan("user"));
//        System.out.println(firebaseConnector.take("user", "-KzCAt7taF9aByOvKJcg"));
    }
}
