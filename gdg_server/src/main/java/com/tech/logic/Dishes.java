/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tech.logic;

/**
 *
 * @author phuongnm
 */
public class Dishes {
        final static public String nameTbl = "Dishes";

        String id;
        String description;
        String name;
        String addr;
        String urlImage;

        public Dishes(String id, String description, String name, String addr, String urlImage) {
                this.id = id;
                this.description = description;
                this.name = name;
                this.addr = addr;
                this.urlImage = urlImage;

        }

        public void setUrlImage(String urlImage) {
                this.urlImage = urlImage;
        }

        public String getUrlImage() {
                return urlImage;
        }

        public String getId() {
                return id;
        }

        public String getDescription() {
                return description;
        }

        public String getName() {
                return name;
        }

        public String getAddr() {
                return addr;
        }

        public void setId(String id) {
                this.id = id;
        }

        public void setDescription(String description) {
                this.description = description;
        }

        public void setName(String name) {
                this.name = name;
        }

        public void setAddr(String addr) {
                this.addr = addr;
        }
        
        public void saveToDb(){
                Db.dbConnector.put(Dishes.nameTbl, Db.gson.toJson(this) );
        }
                
}
