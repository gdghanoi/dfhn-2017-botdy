/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tech.logic;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import com.tech.database.FirebaseConnector;
import io.vertx.core.json.JsonObject;
import javafx.scene.control.Tab;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author phuongnm
 */
public class LogicHelper extends Db {

        public static void installDb() {
//                // user
//                String pass = "123";
//                String passHash = Hashing.sha1().hashString(pass, Charsets.UTF_8).toString();
//                User userClient = new User("botdy", passHash);
//
//                String passS = "456";
//                String passHashS = Hashing.sha1().hashString(passS, Charsets.UTF_8).toString();
//                User userServer = new User("admin", passHashS);
//                System.out.println(dbConnector.put(User.name, userClient.getUserName(),
//                        gson.toJson(userClient)) + "hehe");
//                System.out.println(dbConnector.put(User.name, userServer.getUserName(),
//                        gson.toJson(userServer)) + "hehe");
//
//                // table
//                ArrayList<Table> lstTbl = new ArrayList<>();
//                lstTbl.add(new Table(null, 0, "A", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 1, "A", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 0, "A", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 0, "A", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 0, "B", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 1, "B", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 0, "B", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 0, "B", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 1, "B", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 0, "C", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 0, "C", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 1, "C", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 0, "C", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 0, "D", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 1, "D", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 0, "D", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 0, "D", 4, "Lake view. best service"));
//                lstTbl.add(new Table(null, 1, "D", 4, "Lake view. best service"));
//                lstTbl.forEach(Table::saveToDb);
//
//                // dishes
                List<Dishes> dishesList = new ArrayList<>();
                dishesList.add(new Dishes(null, "Buffet VIP", "Buffet VIP", "28 Nguyen Chi Thanh", "https://image.ibb.co/ccFCsm/1.jpg"));
                dishesList.add(new Dishes(null, "Buffet VIP2", "Buffet VIP2", "28 Tran Duy Hung", "https://image.ibb.co/kDSQCm/1_1.jpg"));
                dishesList.add(new Dishes(null, "Buffet VIP2", "Buffet VIP2", "28 Nguyen Chi Thanh", "https://image.ibb.co/cX7LdR/1_2.jpg"));
                dishesList.add(new Dishes(null, "Buffet VIP2", "Buffet VIP2", "28 Nguyen Chi Thanh", "https://image.ibb.co/bGj0dR/9123d558bd68da56477282b815522309_featured_v2.jpg"));
                dishesList.add(new Dishes(null, "Buffet VIP2", "Buffet VIP2", "28 Tran Duy Hung", "https://image.ibb.co/co8RJR/download.jpg"));
                dishesList.forEach(Dishes::saveToDb);

                // order
                List<Order> orderList = new ArrayList<>();
                orderList.add(new Order("Approved", "11/11/2017", "6:30 PM", "quangbd.hust@gmail.com", "+8412345678", "-KzCyVnIZ9LBg_qXOXNJ", 4, "Debit/Credit Card"));
                orderList.add(new Order("Approved", "11/11/2017", "6:30 PM", "quangbd.hust@gmail.com", "+8412345678", "-KzCyVr1uc2r8FhF8dl1", 4, "Net banking"));
                orderList.forEach(Order::saveToDb);
        }

        public static Boolean checkUser(String userName, String token) {
                String jsonUser = dbConnector.take(User.name, userName);
                User user = gson.fromJson(jsonUser, User.class);

                return user.getToken().equals(token);
        }

        public static String singin(String username, String pass) {
                // call method  get pass 
                Long millis = System.currentTimeMillis();
                String jsonUser = dbConnector.take(User.name, username);
                User user = gson.fromJson(jsonUser, User.class);
                String tokenUser = Hashing.sha1().hashString(username + millis, Charsets.UTF_8).toString();
                user.setToken(tokenUser);

                String dbPass = user.getPassword();

                if (pass.equals(dbPass)) {
                        // save key to db 
                        user.saveToDb();

                        // return result true 
                        JsonObject jsonRet = new JsonObject();
                        jsonRet.put("status", true);
                        jsonRet.put("token", tokenUser);
                        return jsonRet.toString();

                } else {
                        // return result false 
                        JsonObject jsonRet = new JsonObject();
                        jsonRet.put("status", false);
                        jsonRet.put("token", "");
                        return jsonRet.toString();
                }
        }

        public static String getAllTable(String userName, String token) {
                if (LogicHelper.checkUser(userName, token)) {
                        return dbConnector.scan(Table.name);
                } else {
                        System.out.println("Token err");
                        JsonObject jsonRet = new JsonObject();
                        return jsonRet.toString();
                }
        }

        public static String getAllDish(String userName, String token) {
                if (LogicHelper.checkUser(userName, token)) {
                        return dbConnector.scan(Dishes.nameTbl);
                } else {
                        System.out.println("Token err");
                        JsonObject jsonRet = new JsonObject();
                        return jsonRet.toString();
                }
        }

        public static String getAllTableAndDish(String userName, String token) {
                if (LogicHelper.checkUser(userName, token)) {
                        JSONObject tables = new JSONObject(dbConnector.scan(Table.name));
                        JSONArray tableArrays = new JSONArray();
                        for (String key : tables.keySet()) {
                                JSONObject table = tables.getJSONObject(key);
                                table.put("id", key);
                                tableArrays.put(table);
                        }

                        JSONObject dishes = new JSONObject(dbConnector.scan(Dishes.nameTbl));
                        JSONArray dishArrays = new JSONArray();
                        for (String key : dishes.keySet()) {
                                JSONObject dish = dishes.getJSONObject(key);
                                dish.put("id", key);
                                dishArrays.put(dish);
                        }

                        JSONObject jsonRet = new JSONObject();
                        jsonRet.put("tables", tableArrays);
                        jsonRet.put("dishes", dishArrays);
                        return jsonRet.toString();
                } else {
                        System.out.println("Token err");
                        JsonObject jsonRet = new JsonObject();
                        return jsonRet.toString();
                }
        }

        public static String getAllOrderWaiting(String userName, String token) {
                if (LogicHelper.checkUser(userName, token)) {
                        JSONObject orders = new JSONObject(dbConnector.scan(Order.name));
                        JSONArray orderArrays = new JSONArray();
                        for (String key : orders.keySet()) {
                                JSONObject table = orders.getJSONObject(key);
                                if (table.getString("status").equals("Waiting")) {
                                        table.put("id", key);
                                        orderArrays.put(table);
                                }
                        }
                        return orderArrays.toString();
                } else {
                        System.out.println("Token err");
                        JsonObject jsonRet = new JsonObject();
                        return jsonRet.toString();
                }
        }

        public static String getAllOrderHistory(String userName, String token) {
                if (LogicHelper.checkUser(userName, token)) {
                        JSONObject orders = new JSONObject(dbConnector.scan(Order.name));
                        JSONArray orderArrays = new JSONArray();
                        for (String key : orders.keySet()) {
                                JSONObject table = orders.getJSONObject(key);
                                if (!table.getString("status").equals("Waiting")) {
                                        table.put("id", key);
                                        orderArrays.put(table);
                                }
                        }
                        return orderArrays.toString();
                } else {
                        System.out.println("Token err");
                        JsonObject jsonRet = new JsonObject();
                        return jsonRet.toString();
                }
        }

        public static String requireOrder(String userName, String token,
                String tableId, String date, String time, String phone,
                String email, String idDish, String cardNo, String expiry, String ccv
        ) {
                if (LogicHelper.checkUser(userName, token)) {
                        // save table 
                        String jsonTable = dbConnector.take(Table.name, tableId);
                        Table table = gson.fromJson(jsonTable, Table.class);
                        table.setStatus(1);
                        table.saveToDbReplace(tableId);
                        
                        // save order
                        Order order = new Order("Waiting", date, time, email, phone,
                                idDish, 4, "Cash");
                        order.saveToDb();
                        
                        // 
                        JSONObject jsonRet = new JSONObject();
                        jsonRet.put("status", true);
                        return jsonRet.toString();
                } else {
                        System.out.println("Token err");
                        JSONObject jsonRet = new JSONObject();
                        jsonRet.put("status", false);
                        return jsonRet.toString();
                }
        }

        public static Boolean order(String username, String pass) {
                // call method  get pass
                String dbPass = ""; // getPass(username)

                return pass.equals(dbPass);
        }
}
