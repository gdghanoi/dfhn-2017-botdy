/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tech.logic;

/**
 * @author phuongnm
 */
public class Order {
    final static public String name = "Order";

    private String status;
    private String date;
    private String time;
    private String email;
    private String phoneNumber;
    private String idDishes;
    private Integer numOfSheet;
    private String paymentType;

    public Order(String status, String date, String time, String email, String phoneNumber, String idDishes, Integer numOfSheet, String paymentType) {
        this.status = status;
        this.date = date;
        this.time = time;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.idDishes = idDishes;
        this.numOfSheet = numOfSheet;
        this.paymentType = paymentType;
    }

    public String getStatus() {
        return status;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getIdDishes() {
        return idDishes;
    }

    public Integer getNumOfSheet() {
        return numOfSheet;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setIdDishes(String idDishes) {
        this.idDishes = idDishes;
    }

    public void setNumOfSheet(Integer numOfSheet) {
        this.numOfSheet = numOfSheet;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public void saveToDb() {
        Db.dbConnector.put(Order.name, Db.gson.toJson(this));
    }
}
