/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tech.logic;

/**
 *
 * @author phuongnm
 */
public class Result {
        String status ;
        String token;

        public Result(String status, String token) {
                this.status = status;
                this.token = token;
        }

        public String getStatus() {
                return status;
        }

        public String getToken() {
                return token;
        }

        public void setStatus(String status) {
                this.status = status;
        }

        public void setToken(String token) {
                this.token = token;
        }
        
        
}
