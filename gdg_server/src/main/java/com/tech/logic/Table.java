/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tech.logic;

/**
 *
 * @author phuongnm
 */
public class Table extends Db{
        final static public String name = "Table";

        private String id;
        private Integer status;
        private String floorInfo;
        private Integer numberOfseet;
        private String vipValue;

        public Table(String id, Integer status, String floorInfo, Integer NumberOfseet, String VipValue) {
                this.id = id;
                this.status = status;
                this.floorInfo = floorInfo;
                this.numberOfseet = NumberOfseet;
                this.vipValue = VipValue;
        }

        public String getId() {
                return id;
        }

        public Integer getStatus() {
                return status;
        }

        public String getFloorInfo() {
                return floorInfo;
        }

        public Integer getNumberOfseet() {
                return numberOfseet;
        }

        public String getVipValue() {
                return vipValue;
        }

        public void setStatus(Integer status) {
                this.status = status;
        }

        public void setFloorInfo(String floorInfo) {
                this.floorInfo = floorInfo;
        }

        public void setNumberOfseet(Integer NumberOfseet) {
                this.numberOfseet = NumberOfseet;
        }

        public void setVipValue(String VipValue) {
                this.vipValue = VipValue;
        }
        
        public void saveToDb(){
                Db.dbConnector.put(Table.name, Db.gson.toJson(this) );
        }
        
        public void saveToDbReplace(String id){
                Db.dbConnector.put(Table.name, id, Db.gson.toJson(this) );
        }
}
