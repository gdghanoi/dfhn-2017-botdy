/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tech.logic;

/**
 *
 * @author phuongnm
 */
public class User extends Db{
        final static public String name = "User";
        String userName ;
        String password ;
        String token ;

        public User( String userName, String password) {
                this.userName = userName;
                this.password = password;
        }

        public User(String userName, String password, String token) {
                this.userName = userName;
                this.password = password;
                this.token = token;
        }

        public String getToken() {
                return token;
        }

        public void setToken(String token) {
                this.token = token;
        }


        public String getUserName() {
                return userName;
        }

        public String getPassword() {
                return password;
        }


        public void setUserName(String userName) {
                this.userName = userName;
        }

        public void setPassword(String password) {
                this.password = password;
        }
        
        @Override
        public void saveToDb(){
                System.out.println(""+Db.gson.toJson(this));
                Db.dbConnector.put(User.name, this.getUserName(), 
                        Db.gson.toJson(this) );
        }
        
        @Override
        public String toString() {
                return "User{"  + "userName=" + userName + ", password=" + password + '}';
        }
        
}
