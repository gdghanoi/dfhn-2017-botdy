package com.tech.server;

import com.tech.logic.LogicHelper;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import javafx.scene.transform.Rotate;
import org.json.JSONArray;

public class Server extends AbstractVerticle {

    @Override
    public void start() throws Exception {
        // router
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.route().handler(CookieHandler.create());
        router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));

        router.route(HttpMethod.GET, "/hello").handler(this::hello);
        router.route(HttpMethod.POST, "/login").handler(this::login);
        router.route(HttpMethod.POST, "/list-table").handler(this::getListTable);
        router.route(HttpMethod.POST, "/payment").handler(this::payment);

        // listen
        vertx.createHttpServer().requestHandler(router::accept).listen(1505);
    }

    private void hello(RoutingContext routingContext){
        HttpServerResponse response = routingContext.response();
        response.putHeader("Access-Control-Allow-Origin", "*");
        response.end("hello");
    }

    private void login(RoutingContext routingContext){
        HttpServerResponse response = routingContext.response();
        response.putHeader("Access-Control-Allow-Origin", "*");
        System.out.println(routingContext.getBodyAsString());
        JsonObject jsonObject = new JsonObject(routingContext.getBodyAsString());
        String username = jsonObject.getString("username");
        String password = jsonObject.getString("password");
        response.end(LogicHelper.singin(username, password));
    }

    private void getListTable(RoutingContext routingContext){
        HttpServerResponse response = routingContext.response();
        response.putHeader("Access-Control-Allow-Origin", "*");
        System.out.println(routingContext.getBodyAsString());
        JsonObject jsonObject = new JsonObject(routingContext.getBodyAsString());
        String username = jsonObject.getString("username");
        String token = jsonObject.getString("token");
        response.end(LogicHelper.getAllTableAndDish(username, token));
    }

    private void payment(RoutingContext routingContext){
        HttpServerResponse response = routingContext.response();
        response.putHeader("Access-Control-Allow-Origin", "*");
        System.out.println(routingContext.getBodyAsString());
        JsonObject jsonObject = new JsonObject(routingContext.getBodyAsString());
        String username = jsonObject.getString("username");
        String token = jsonObject.getString("token");
        String tableId = jsonObject.getString("tableId");
        String date = jsonObject.getString("date");
        String time = jsonObject.getString("time");
        String phone = jsonObject.getString("phone");
        String email = jsonObject.getString("email");
        String cardNo = jsonObject.getString("cardNo");
        String expiry = jsonObject.getString("expiry");
        String ccv = jsonObject.getString("ccv");
        String dishId = jsonObject.getString("dishId");



        response.end(LogicHelper.requireOrder(username, token, tableId, date, time,
                phone, email, dishId, cardNo, expiry, ccv));
    }

    private void getOrderWaiting(RoutingContext routingContext){
        HttpServerResponse response = routingContext.response();
        response.putHeader("Access-Control-Allow-Origin", "*");
        System.out.println(routingContext.getBodyAsString());
        JsonObject jsonObject = new JsonObject(routingContext.getBodyAsString());
        String username = jsonObject.getString("username");
        String token = jsonObject.getString("token");

        response.end(LogicHelper.getAllOrderWaiting(username, token));
    }

    private void getOrderHistory(RoutingContext routingContext){
        HttpServerResponse response = routingContext.response();
        response.putHeader("Access-Control-Allow-Origin", "*");
        System.out.println(routingContext.getBodyAsString());
        JsonObject jsonObject = new JsonObject(routingContext.getBodyAsString());
        String username = jsonObject.getString("username");
        String token = jsonObject.getString("token");

        response.end(LogicHelper.getAllOrderWaiting(username, token));
    }

    public static void main(String[] args) {
        VertxOptions vertxOptions = new VertxOptions();
        DeploymentOptions options = new DeploymentOptions();
        options.setWorker(true);
        Vertx vertx = Vertx.vertx(vertxOptions);
        vertx.deployVerticle(Server.class.getName(), options);
    }
}
